import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchTiles, toggleTile, STATUS_LOOSE, STATUS_WIN } from '../modules/play';
import { setHighScore, saveHighScore, enableScoreForm } from '../modules/score';
import Board from '../components/Board/Board';
import Header from '../components/Header/Header';
import Score from '../components/Score/Score';

class GameScreen extends Component {
    componentDidMount() {
        this.props.fetchTiles();
        this.size = Math.pow(document.getElementById('board').offsetWidth, 2);
    }

    render() {
        let screenDisplay =
            (<Board
                tiles={this.props.tiles}
                size={this.size}
                onTileClick={this.props.onTileClick}
                step={this.props.step}
            />);

        if ([STATUS_LOOSE, STATUS_WIN].indexOf(this.props.status) > -1) {
            screenDisplay = (
                <Score
                    form={this.props.form}
                    list={this.props.list}
                    onHighScoreChange={this.props.onHighScoreChange}
                    onPlayClick={this.props.fetchTiles}
                    onSaveClick={this.props.onSaveClick.bind(this, this.props.step)}
                    player={this.props.player}
                />);
        }

        return (
            <div>
                <Header
                    status={this.props.status}
                    step={this.props.step}
                />
                {screenDisplay}
            </div>
        );
    }
}

GameScreen.propTypes = {
    fetchTiles: PropTypes.func,
    form: PropTypes.bool,
    list: React.PropTypes.arrayOf(React.PropTypes.object),
    onHighScoreChange: PropTypes.func,
    onSaveClick: PropTypes.func,
    onTileClick: PropTypes.func,
    player: PropTypes.string,
    status: PropTypes.string,
    step: PropTypes.number,
    tiles: React.PropTypes.arrayOf(React.PropTypes.object)
};

const mapStateToProps = state => ({
    form: state.score.form,
    list: state.score.list,
    player: state.score.player,
    status: state.play.status,
    step: state.play.step || 0,
    tiles: state.play.tiles || []
});

const mapDispatchToProps = dispatch => ({
    fetchTiles: () => {
        dispatch(fetchTiles());
        dispatch(enableScoreForm());
    },
    onHighScoreChange: event => dispatch(setHighScore(event)),
    onTileClick: index => dispatch(toggleTile(index)),
    onSaveClick: step => dispatch(saveHighScore(step))
});

export default connect(mapStateToProps, mapDispatchToProps)(GameScreen);
