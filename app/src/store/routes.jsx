import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Default from '../layouts/Default';
import GameScreen from '../containers/GameScreen';

export default () => (
    <Route component={Default} path='/'>
        <IndexRoute component={GameScreen} />
        <Route component={GameScreen} path='*' />
    </Route>
);
