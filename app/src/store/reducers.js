import { combineReducers } from 'redux';
import play from '../modules/play';
import score from '../modules/score';

const rootReducer = combineReducers({
    play,
    score,
});

export default rootReducer;
