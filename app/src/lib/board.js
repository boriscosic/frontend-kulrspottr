import fill from 'lodash/fill';

export default class Board {
    static generate(tileCount, lastColor) {
        const items = [];
        const color = this.color(lastColor);

        fill(Array(Math.pow(tileCount, 2))).forEach(() =>
            items.push({
                playable: false,
                color
            })
        );

        items[Math.floor(Math.random() * items.length)].playable = true;
        return items;
    }

    static color(lastColor) {
        const colors = ['#00aedb', '#a200ff', '#f47835', '#d41243', '#8ec127'];
        const colorPick = () => colors[Math.floor(Math.random() * colors.length)];
        let newColor = colorPick();
        while (newColor === lastColor) {
            newColor = colorPick();
        }
        return newColor;
    }

    static isValid(tile) {
        return tile.playable;
    }
}
