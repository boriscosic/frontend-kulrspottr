import store from 'store';

export const SET_HIGH_SCORE = 'SET_HIGH_SCORE';
export const ENABLE_SCORE_FORM = 'ENABLE_SCORE_FORM';
export const SAVE_HIGH_SCORE = 'SAVE_HIGH_SCORE';

export function setHighScore(event) {
    return { type: SET_HIGH_SCORE, player: event.target.value };
}

export function saveHighScore(step) {
    return { type: SAVE_HIGH_SCORE, step };
}

export function enableScoreForm() {
    return { type: ENABLE_SCORE_FORM };
}

const initialState = {
    player: '',
    form: true,
    list: store.get('KulrSpottrScore') || []
};

export default function score(state = initialState, action) {
    switch (action.type) {

    case SET_HIGH_SCORE: {
        let player = state.player;
        player = action.player;
        return { ...state, player };
    }

    case SAVE_HIGH_SCORE: {
        const list = Object.assign([], state.list);
        let form = state.form;
        list.push({
            player: state.player,
            step: action.step
        });

        list.sort((a, b) => (a.step < b.step) ? 1 : ((b.step < a.step) ? -1 : 0)); // eslint-disable-line
        form = false;

        const newList = list.slice(0, 10);
        store.set('KulrSpottrScore', newList);
        return { ...state, list: newList, form, player: '' };
    }

    case ENABLE_SCORE_FORM: {
        return { ...state, form: true };
    }

    default: {
        return state;
    }
    }
}
