import { assert } from 'chai';
import reducer, { fetchTiles, toggleTile, STATUS_LOOSE, FETCH_TILES,
    TOGGLE_TILE, STATUS_NEW } from './play';
import Board from '../lib/Board';

describe('play module', () => {
    let color = null;
    let tiles = null;

    describe('actions', () => {
        describe('fetchTiles()', () => {
            it('should create FETCH_TILES action', () => {
                const action = { type: FETCH_TILES };
                assert.deepEqual(fetchTiles(), action);
            });
        });

        describe('toggleTile()', () => {
            it('should create TOGGLE_TILE action', () => {
                const action = { type: TOGGLE_TILE, tile: 1 };
                assert.deepEqual(toggleTile(1), action);
            });
        });
    });

    describe('reducers', () => {
        describe('board lib', () => {
            it('should get a new color each time', () => {
                const color1 = Board.color();
                color = Board.color(color1);
                assert.notEqual(color1, color);
            });

            it('should get a new tileset each time', () => {
                const tiles1 = Board.generate(9);
                tiles = Board.generate(9);
                assert.notEqual(tiles1, tiles);
            });
        });

        describe('FETCH_TILES', () => {
            it('should create tiles for the game', () => {
                const action = { type: FETCH_TILES };
                const tileCount = 2;
                const expectedState = {
                    tiles,
                    status: STATUS_NEW,
                    tileCount,
                    step: 0,
                    color
                };
                const resultState = reducer({}, action);
                resultState.color = color;
                resultState.tiles = tiles;

                assert.deepEqual(resultState, expectedState);
            });
        });

        describe('TOGGLE_TILE', () => {
            it('should loose the game if wrong tile is clicked', () => {
                const action = { type: TOGGLE_TILE, tile: 1 };
                const initialState = {
                    tiles: [
                        { playable: true },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false }
                    ],
                    status: STATUS_NEW,
                    tileCount: 2,
                    step: 0
                };
                const expectedState = {
                    tiles: [
                        { playable: true },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false }
                    ],
                    status: STATUS_LOOSE,
                    tileCount: 2,
                    step: 0
                };
                const resultState = reducer(initialState, action);
                assert.deepEqual(resultState, expectedState);
            });

            it('should win the game if right tile is clicked', () => {
                const action = { type: TOGGLE_TILE, tile: 0 };
                const initialState = {
                    tiles: [
                        { playable: true },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false },
                        { playable: false }
                    ],
                    status: STATUS_NEW,
                    tileCount: 2,
                    step: 0
                };

                const resultState = reducer(initialState, action);
                assert.notEqual(resultState.tileCount, initialState.tileCount, 'Should have more tiles');
                assert.equal(resultState.status, initialState.status, 'Should have new status');
                assert.notEqual(resultState.step, initialState.step, 'Should have a new step');
            });
        });
    });
});
