import Board from '../lib/board';

export const FETCH_TILES = 'FETCH_TILES';
export const TOGGLE_TILE = 'TOGGLE_TILE';
export const STATUS_NEW = 'STATUS_NEW';
export const STATUS_LOOSE = 'STATUS_LOOSE';
export const STATUS_WIN = 'STATUS_WIN';

export function fetchTiles() {
    return { type: FETCH_TILES };
}

export function toggleTile(tile) {
    return { type: TOGGLE_TILE, tile };
}

export default function play(state = {}, action) {
    switch (action.type) {

    case FETCH_TILES: {
        const tileCount = STARTING_TILES_PER_ROW;
        return { ...state,
            tiles: Board.generate(tileCount, null),
            color: Board.color(null),
            status: STATUS_NEW,
            tileCount,
            step: 0
        };
    }

    case TOGGLE_TILE: {
        if (Board.isValid(state.tiles[action.tile]) && state.step < GAME_STEPS) {
            return { ...state,
                tiles: Board.generate(state.tileCount + 1, state.tiles[action.tile].color),
                status: STATUS_NEW,
                tileCount: state.tileCount + 1,
                step: state.step + 1
            };
        } else if (state.step === GAME_STEPS) {
            return { ...state, status: STATUS_WIN };
        }

        return { ...state, status: STATUS_LOOSE };
    }

    default: {
        return state;
    }
    }
}
