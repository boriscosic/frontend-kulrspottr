import React, { PropTypes } from 'react';
import Tile from '../Tile/Tile';
import './Board.scss';

const Board = props => (
    <div className={`board board-step-${props.step}`} id='board' data-steps={GAME_STEPS}>
        { props.tiles.map((tile, index) =>
            <Tile
                color={tile.color}
                dimensions={parseInt(Math.sqrt(props.size / props.tiles.length), 0)}
                index={index}
                key={`tile_${index}`}
                onTileClick={props.onTileClick}
                playable={tile.playable}
            />)
        }

        <p className='help-block'>
            {'Click on a tile that has a different color.'}
        </p>
    </div>
);

Board.propTypes = {
    onTileClick: PropTypes.func,
    step: PropTypes.number,
    tiles: React.PropTypes.arrayOf(React.PropTypes.object)
};

export default Board;
