/* eslint-disable react/jsx-filename-extension */
import { assert } from 'chai';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import Tile from './Tile';

describe('<Tile />', () => {
    it('component clasname should be tile--1', () => {
        const onTileClick = sinon.spy();
        const wrapper = shallow(<Tile color={'#ff0000'} dimensions={50} playable index={1} onTileClick={onTileClick} />);
        assert.equal(wrapper.find('.tile-playable').length, 1);
    });

    it('component should have inline style set from props', () => {
        const onTileClick = sinon.spy();
        const wrapper = shallow(<Tile color={'#ff0000'} dimensions={50} playable={false} index={1} onTileClick={onTileClick} />);
        assert.deepEqual(wrapper.find('.tile-disabled').props().style, { backgroundColor: '#ff0000', height: '50px', width: '50px' });
    });

    it('component clasname should be tile-0', () => {
        const onTileClick = sinon.spy();
        const wrapper = shallow(<Tile color={'#ff0000'} dimensions={50} playable={false} index={1} onTileClick={onTileClick} />);
        assert.equal(wrapper.find('.tile-disabled').length, 1);
    });

    it('component click should trigger an action', () => {
        const onTileClick = sinon.spy();
        const wrapper = shallow(<Tile color={'#ff0000'} dimensions={50} playable={false} index={1} onTileClick={onTileClick} />);
        wrapper.find('.tile').simulate('click');
        assert.equal(onTileClick.args[0][0], 1);
    });
});
