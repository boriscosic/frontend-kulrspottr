import React, { PropTypes } from 'react';
import './Tile.scss';

const Tile = props => (
    <button
        className={`tile tile-${props.playable ? 'playable' : 'disabled'}`}
        onClick={props.onTileClick.bind(this, props.index)}
        style={{
            backgroundColor: props.color,
            height: `${props.dimensions}px`,
            width: `${props.dimensions}px`
        }}
    />
);

Tile.propTypes = {
    color: PropTypes.string,
    dimensions: PropTypes.number,
    index: PropTypes.number,
    onTileClick: PropTypes.func,
    playable: PropTypes.bool
};

export default Tile;
