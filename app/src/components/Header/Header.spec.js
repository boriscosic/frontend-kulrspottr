/* eslint-disable react/jsx-filename-extension */
import { assert } from 'chai';
import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';

describe('<Header />', () => {
    it('component should contain step 0', () => {
        const wrapper = shallow(<Header status={'STATUS_NEW'} step={0} />);
        assert.equal(wrapper.find('h2').length, 1);
        assert.equal(wrapper.find('h2').text(), 'Step 0');
    });

    it('component should show on game over', () => {
        const wrapper = shallow(<Header status={'STATUS_LOOSE'} step={5} />);
        assert.equal(wrapper.find('h2').length, 1);
        assert.equal(wrapper.find('h2').text(), 'Step 5 - Game Over');
    });

    it('component should show on game win', () => {
        const wrapper = shallow(<Header status={'STATUS_WIN'} step={10} />);
        assert.equal(wrapper.find('h2').length, 1);
        assert.equal(wrapper.find('h2').text(), 'Step 10 - You Win!');
    });
});
