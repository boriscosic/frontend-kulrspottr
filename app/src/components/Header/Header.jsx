import React, { PropTypes } from 'react';
import './Header.scss';
import { STATUS_LOOSE, STATUS_WIN } from '../../modules/play';

const Header = props => (
    <header>
        <h1>{'KulrSpottr'}</h1>
        <h2>
            Step {props.step}
            {props.status === STATUS_LOOSE && ' - Game Over'}
            {props.status === STATUS_WIN && ' - You Win!'}
        </h2>
    </header>
);

Header.propTypes = {
    status: PropTypes.string,
    step: PropTypes.number
};

export default Header;
