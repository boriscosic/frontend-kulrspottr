/* eslint-disable react/jsx-filename-extension */
import { assert } from 'chai';
import React from 'react';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import Score from './Score';

describe('<Score />', () => {
    const onHighScoreChange = sinon.spy();
    const onPlayClick = sinon.spy();
    const onSaveClick = sinon.spy();

    it('component form should be visible', () => {
        const wrapper = shallow(
            <Score
                form
                list={[]}
                onHighScoreChange={onHighScoreChange}
                onPlayClick={onPlayClick}
                onSaveClick={onSaveClick}
                player={''}
            />
        );
        assert.equal(wrapper.find('form').length, 1);
    });

    it('component should not have any high scores', () => {
        const wrapper = shallow(
            <Score
                form
                list={[]}
                onHighScoreChange={onHighScoreChange}
                onPlayClick={onPlayClick}
                onSaveClick={onSaveClick}
                player={''}
            />
        );
        assert.equal(wrapper.find('.list').text(), 'First high score!');
    });

    it('component submit button should be disabled', () => {
        const wrapper = shallow(
            <Score
                form
                list={[]}
                onHighScoreChange={onHighScoreChange}
                onPlayClick={onPlayClick}
                onSaveClick={onSaveClick}
                player={''}
            />
        );
        assert.equal(Object.keys(wrapper.find('[name="saveHighScore"]').props()).indexOf('disabled') !== -1, true);
    });

    it('component should have scores', () => {
        const wrapper = shallow(
            <Score
                form
                list={[{ step: 1, player: 'boris' }, { step: 0, player: 'john' }]}
                onHighScoreChange={onHighScoreChange}
                onPlayClick={onPlayClick}
                onSaveClick={onSaveClick}
                player={''}
            />
        );
        assert.equal(wrapper.find('.player_score').length, 2);
    });

    it('component should be able to submit a score', () => {
        const wrapper = shallow(
            <Score
                form
                list={[{ step: 1, player: 'boris' }, { step: 0, player: 'john' }]}
                onHighScoreChange={onHighScoreChange}
                onPlayClick={onPlayClick}
                onSaveClick={onSaveClick}
                player={'testing'}
            />
        );
        wrapper.find('[name="saveHighScore"]').simulate('click');
        assert.equal(onSaveClick.args[0].length, 0);
    });
});
