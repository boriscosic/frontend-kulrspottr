import React, { PropTypes } from 'react';
import './Score.scss';

const Score = props => (
    <div className='score'>
        {(props.list && (props.list.length === 0 || props.form)) &&
            (<form>
                <label htmlFor='hallOfFame'>Enter a name for Hall of Fame!</label>
                <div>
                    <input
                        id="yourName"
                        onChange={props.onHighScoreChange.bind(this)}
                        placeholder='Enter your name...'
                        type='text'
                    />
                </div>
                <input
                    disabled={props.player.length === 0}
                    id='saveHighScore'
                    onClick={props.onSaveClick}
                    name='saveHighScore'
                    type='button'
                    value='Save'
                />
            </form>)
        }
        <div className='list'>
            {props.list && props.list.length > 0 ?
                props.list.map((score, index) => (
                    <div key={`score${index}`} className='player_score'>
                        <strong>{index + 1}. </strong>
                        {`Step: ${score.step} - Player: ${score.player}`}
                    </div>))
                : 'First high score!'
            }
        </div>
        <p>
            <input
                onClick={props.onPlayClick}
                type='button'
                value='Play Again'
            />
        </p>
    </div>
);

Score.propTypes = {
    form: PropTypes.bool,
    list: React.PropTypes.arrayOf(React.PropTypes.object),
    onHighScoreChange: PropTypes.func,
    onPlayClick: PropTypes.func,
    onSaveClick: PropTypes.func,
    player: PropTypes.string
};

export default Score;
