const Webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './app/src/containers/AppContainer.jsx',
    output: {
        path: __dirname + '/app/dist',
        filename: 'bundle.js',
        publicPath: '/'
    },
    resolve: {
        modulesDirectories: [
            'app/src',
            'node_modules'
        ],
        extensions: ['', '.js', '.jsx']
    },
    module: {
        loaders: [
            { test: /\.jsx?$/, loader: 'babel', exclude: /node_modules/ },
            { test: /\.scss$/, loaders: ['style', 'css', 'sass'] }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'app/src/static/index.ejs',
        }),
        new Webpack.DefinePlugin({
            STARTING_TILES_PER_ROW: 2,
            GAME_STEPS: 11,
        })
    ]
}
