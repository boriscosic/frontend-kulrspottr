/* eslint-disable */
import { assert } from 'chai';
import async from 'async';

describe('client test', function() {
    it('user can finish the game', function() {
        browser.url('http://localhost:8000');
        assert.equal(browser.getTitle(), 'KulrSpottr');

        browser.waitForVisible('#board', 2000);
        let testSeries = [];
        let steps = parseInt(browser.getAttribute('#board', 'data-steps'));

        // synchronously test the game dynamically based on steps
        Array(steps+1).fill(0).forEach((val, step) => {
            testSeries.push(
                (cb) => {
                    browser.waitForVisible(`.board-step-${step}`, 1000);
                    const tiles = Math.pow(step + 2, 2);
                    assert.equal(browser.elements('.tile').value.length, tiles);
                    browser.click('.tile-playable');
                    cb();
                }
            );
        });

        // test the end form and local storage
        testSeries.push(
            (cb) => {
                browser.waitForVisible('.score', 1000);
                browser.setValue('#yourName', 'test user');
                browser.click('#saveHighScore');
                browser.waitForVisible(`.list`, 1000);
                assert.notEqual(browser.getText('.list').indexOf('test user'), -1);
                assert.notEqual(browser.localStorageSize(), 0);

                browser.localStorage('GET', 'KulrSpottrScore', (res) => {
                    assert.deepEqual(JSON.parse(res), [{ player: 'test user', step: 'steps' }]);
                    cb();
                });
            }
        );
        async.series(testSeries);
    });
});
