FROM node:6

ENV WD /opt/web

WORKDIR $WD
COPY . $WD
RUN npm install